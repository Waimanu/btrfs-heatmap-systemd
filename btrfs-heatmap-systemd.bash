#!/usr/bin/env bash
set -e

## Helpful functions
function __btrfs_heatmap_systemd_help() {
  printf "Usage: btrfs-heatmap-systemd xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
  exit 1
}

function __md5sumShort() {
  md5sum "${1}" | grep --perl-regex --only-match '^\S+'
}

echo "${1}" | grep -qP '[a-z0-9]{8}-([a-z0-9]{4}-){3}[a-z0-9]{12}' ||
__btrfs_heatmap_systemd_help
btrfs_volume_uuid="${1}"

heatmap_home=/var/lib/heatmap

heatmap_volume_path="${heatmap_home}/${btrfs_volume_uuid}"
heatmap_work_home="${heatmap_volume_path}/.heatmap"
heatmap_work_path="${heatmap_work_home}/export"

test -d "${heatmap_volume_path}" ||
mkdir -p "${heatmap_volume_path}"

mountpoint -q "${heatmap_volume_path}" ||
mount "UUID=${btrfs_volume_uuid}" "${heatmap_volume_path}"

test -d "${heatmap_work_home}" ||
btrfs subvolume create "${heatmap_work_home}"

test -d "${heatmap_work_path}" ||
mkdir "${heatmap_work_path}"

cd "${heatmap_work_path}"
_png_file_name="$(
  btrfs-heatmap "${heatmap_volume_path}"  |
  grep            \
    --perl-regex  \
    --only-match \
    '^pngfile\s+\K\S+$'
)"

export _png_file_name

if [ -f ./last_image ]; then
  _last_md5="$(__md5sumShort "./last_image")"
  _current_md5="$(__md5sumShort "${_png_file_name}")"

  [ "${_last_md5}" == "${_current_md5}" ] &&
  cp --force --reflink=always "./last_image" "${_png_file_name}"
fi

ln --symbolic --force "${_png_file_name}" "./last_image"
