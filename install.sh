#!/usr/bin/env bash

systemd_system_dir="/etc/systemd/system"

cp --force ./btrfs-heatmap-systemd.bash /usr/local/bin/btrfs-heatmap-systemd
cp ./btrfs-heatmap@.service "${systemd_system_dir}/"
cp ./btrfs-heatmap@.timer "${systemd_system_dir}/"
systemctl daemon-reload
